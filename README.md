
# Prototype code for remote access to an edge device

This software is provided under the MIT Licence. See
[`COPYING`](COPYING). Currently we are not accepting contributions to
this project so please feel free to fork it. We appreciate your
interest and may accept contributions in the future.

## Introduction

This software allows a developer to access a remote edge device, for
debugging, in a way that closely resembles `ssh` or `docker exec`,
even when both the edge device and the developer's machine are behind
firewalls. It does this using a “comms” service running on a publicly
accessible system, such as a cloud server, and three SSH connections:
one from the edge device to the comms service, one from the
developer's machine to the comms service, and a third one that goes
from the developer's machine through the tunnel created by the other
two connections right through to the edge device.

The software consists of three components:

* The “comms” service, which is deployed on a publicly accessible
  system such as a cloud server. This may be shared by multiple
  developers and multiple connections, so it has a REST API for
  setting up accounts and uploading SSH keys. As a consequence, it
  uses two ports for incoming connections: one for HTTP and one for
  SSH.

* The “debug container”, a privileged container that is deployed to
  the edge device using Kubernetes. This container opens an SSH
  connection to the comms service using SSH with port forwarding from
  a Unix-domain socket inside the comms container to an SSH daemon
  running inside the debug container.

* A script, `rdebug`, that the developer runs on their machine. This
  script invokes SSH twice: once to connect to the comms service
  (where a special login shell invokes `socat`) and once to connect
  through the tunnel that has been created right through to the debug
  container (where a special login shell invokes `nsenter` or
  `docker`).

## Quick start: demo

For a compelling demo you need at least three machines: the “edge
device”, the “comms server”, and the “developer's machine”. However,
the instructions below should work just the same if some or all of
those machines are the same machine.

The first step is to build and deploy the comms container. The comms
container would normally be deployed on some cloud infrastructure with
a front-end to perform authentication but in this case we are making
the service available to anyone who can access ports 10022 and 10080
on the “comms server” these commands are run on:

```
docker build -t comms comms
docker run -it -p 10022:22/tcp -p 10080:80/tcp comms
```

The next step is to generate three key pairs. This is best done on the
developer's machine. If that system has a sufficiently recent SSH (for
example, OpenSSH 6.5 of Jan 2014) then we can use Ed25519:

```
ssh-keygen -t ed25519 -C client -P '' -m PEM -f client
ssh-keygen -t ed25519 -C server -P '' -m PEM -f server
ssh-keygen -t ed25519 -C dev -P '' -m PEM -f dev
```

The server key is used for the edge device to connect to the comms
container, the client key is used for the developer's machine to
connect to the comms container, and the dev key is used for the
developer to connect to the edge device through the tunnel created
with the other two connections.

The comms container needs the server and client public keys. We
provide them using the comms container's REST API. Run this on the
developer's machine or wherever the keys were generated:

```
curl -L -X POST -H 'Content-Type: application/json' \
  -H 'X-Auth-Account-Id: boss' -d \
  '{ "user":"bob", "expiry": "'$(date -u -d '2038-01-20' '+%s')'",
     "ckey": "'"$(cat client.pub)"'", "skey": "'"$(cat server.pub)"'"}' \
  http://localhost:10080/create_user
```

The response should be `{"user":"bob"}`, indicating that we were given
the username we asked for. If we had omitted the `"user":"bob"` from
the JSON then a username would have been invented for us.

We can now build and deploy the debug container onto our edge device.
The debug container will be deployed using Kubernetes, so the usual
procedure would be to upload the Docker image to a registry and
configure the edge device with whatever secrets are required to pull
from the registry. The details of that will depend on precisely how
Kubernetes has been deployed, so in this demo the image is built on
the edge device so that it does not need to be pulled. So run this on
the edge device:

```
docker build -t debug:test debug
```

The debug container image is rather unusual. It containts no binaries
and relies on host binaries being mounted into it. It is therefore
architecture-independent, but not OS-independent: the host system must
have certain versions of certain programs in the expected places, for
example `/usr/bin/sshd`. An advantage of this approach is that the
image is tiny and can be deployed onto a resource-limited device
without perturbing it much.

Since some of the potential substitutions are non-trivial, there is a
script for generating the YAML for Kubernetes. The debug container
requires two of the SSH keys generated above, and the values `10022`
and `bob` come from how the comms container was configured. `NODENAME`
must be replaced with the name of the edge device, and `COMMSHOST`
with the address of the machine on which the comms container was
deployed. The `debug-yaml` script requires the keys, so run that
command on the developer's machine or wherever the keys were
generated. If because of the way your Kubernetes is set up you need to
run the `kubectl` on a different machine, just copy the `debug.yaml`
to that machine first.

```
debi=debug:test node=NODENAME host=COMMSHOST port=10022 user=bob \
  skey="$(cat server)" dkey="$(cat dev.pub)" ./debug-yaml > debug.yaml
kubectl apply -f debug.yaml
```

If the debug container was successfully deployed it should now be
possible to connect from the developer's machine to the edge device
using the `rdebug` script provided. Replace `COMMSHOST` as above and
run this on the developer's machine:

```
./rdebug bob COMMSHOST 10022 client dev
```

If everything has gone to plan, that should give you a shell prompt on
the edge device.

There are several features not covered by this simple demo. One worth
mentioning is the ability to run commands in a particular container on
the edge device. To do that, obtain the target container's ID using
`kubectl` and add `containerid=...` to the environment of `debug-yaml`
when generating the YAML for deploying the debug container.
