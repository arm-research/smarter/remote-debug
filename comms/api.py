# Remote debug: Python Flask code for comms container REST API.
# Copyright (c) 2020 Arm Limited
# SPDX-License-Identifier: MIT

from flask import Flask
from flask import request
import subprocess

app = Flask(__name__)

creator_header = 'X-Auth-Account-Id'

@app.route('/create_user', methods=['POST'])
def create_user():
    return op('c')

@app.route('/delete_user', methods=['GET'])
def delete_user():
    return op('d')

@app.route('/list_connections', methods=['GET'])
def list_connections():
    return op('s')

@app.route('/list_users', methods=['GET'])
def list_users():
    return op('l')

@app.route('/query_user', methods=['GET'])
def query_user():
    return op('q')

class BadRequest(Exception):
    def __init__(self, message):
        self.message = message

def op(f):
    try:
        return op_with_exceptions(f)
    except BadRequest as e:
        return ('Error: ' + e.message + '\n' , 400,
                { 'Content-Type': 'text/plain' })

def op_with_exceptions(f):
    def creator():
        c = request.headers.get(creator_header)
        if c == None:
            raise BadRequest('Need "' + creator_header + '" header')
        return c
    def user():
        u = request.args.get('user')
        if u == None:
            raise BadRequest('Need "user" parameter')
        return u
    if f == 'c':
        return run(['-c', creator()], request.data)
    elif f == 'd':
        return run(['-d', creator(), user()], '')
    elif f == 'l':
        return run(['-l', creator()], '')
    elif f == 's':
        return run(['-s', creator()], '')
    elif f == 'q':
        return run(['-q', creator(), user()], '')

def run(args, stdin):
    x = subprocess.run(['/comms/bin/useradmin'] + args,
                       input = stdin, stdout = subprocess.PIPE)
    if x.returncode != 0:
        raise BadRequest('failed')
    return x.stdout, 200, { 'Content-Type': 'application/json' }

if __name__ == "__main__":
    app.run()
