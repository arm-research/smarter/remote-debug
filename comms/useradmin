#!/usr/bin/perl

# Remote debug: comms container account/user administration.
# This is invoked by the REST API front end.
# Copyright (c) 2020 Arm Limited
# SPDX-License-Identifier: MIT

use strict;

use DB_File;
use Fcntl qw(:flock);
use JSON;

my $rdir = ''; # Change this for testing.
my $dbfile = "$rdir/comms/user.db";
my $passwd = "$rdir/etc/passwd";

my $passwd_start = (
    "root:x:0:0:root:/root:/bin/sh\n" .
    "sshd:x:22:22:sshd:/dev/null:/sbin/nologin\n"
    );
my $uid_start = 1000;

# Open database:
my %db;
{
    open(my $fd, '>>', $dbfile) || die;
    flock($fd, LOCK_EX) || die;
    my $db = tie(%db, 'DB_File', $dbfile) || die;
}

{
    my $n = $db{'N'};
    if ($n eq '' || rand($n + 100) < 1) {
        &garbage_collect();
    }
}

if ($#ARGV == 0 && $ARGV[0] !~ /^-/) {
    # Implement AuthorizedKeysCommand.
    my $user = $ARGV[0];
    if ($user =~ s/^([cs])-//) {
        my $side = $1;
        my $json = $db{"U$user"};
        if ($json ne '') {
            my $d = decode_json($json);
            if (!&expired($d)) {
                print $$d{"${side}key"}, "\n";
            }
        }
    }
} elsif ($#ARGV == 1 && $ARGV[0] eq '-c') {
    my $creator = $ARGV[1];
    # Create a user.
    my $x = &read_user_data();
    $$x{'creator'} = $creator;
    my $user = $$x{'user'};
    if ($user eq '') {
        $user = &new_user();
        $$x{'user'} = $user;
    }
    my $json = $db{'U' . $$x{'user'}};
    my $uid;
    if ($json ne '') {
        my $d = decode_json($json);
        die if $$d{'creator'} ne $creator && !&expired($d);
        $uid = $$d{'uid'};
        $$x{'uid'} = $uid;
        &kill_all($user);
    } else {
        $uid = &new_uid();
        $$x{'uid'} = $uid;
        open(my $fd1, '>>', "$passwd") || die;
        print $fd1 &passwd_entries($user, $uid);
        mkdir("$rdir/comms/home/$user", 0700);
        chown($uid, 1000, "$rdir/comms/home/$user");
    }
    open(my $fd2, '>', "$rdir/comms/expiry/$user") || die;
    print $fd2 $$x{'expiry'};
    $db{'U' . $$x{'user'}} = encode_json($x);
    my %t;
    foreach (split(/,/, $db{"C$creator"})) {
        $t{$_} = 1;
    }
    if (!$t{$$x{'user'}}) {
        $t{$$x{'user'}} = 1;
        $db{"C$creator"} = join(',', sort keys %t);
    }
    my %r = ( "user", $user );
    print encode_json(\%r);
} elsif ($#ARGV == 2 && $ARGV[0] eq '-d') {
    # Delete a user.
    my $creator = $ARGV[1];
    my $user = $ARGV[2];
    my $json = $db{"U$user"};
    if ($json ne '') {
        my $d = decode_json($json);
        die if $$d{'creator'} ne $creator && !&expired($d);
        $$d{'expiry'} = 0;
        $db{"U$user"} = encode_json($d);
        &kill_all($user);
    }
    open(my $fd2, '>', "$rdir/comms/expiry/$user") || die;
    print $fd2 "0";
} elsif ($#ARGV == 0 && $ARGV[0] eq '-g') {
    &garbage_collect();
} elsif ($#ARGV == 1 && $ARGV[0] =~ /^-[ls]$/) {
    # List a creator's users or connections.
    my $op_s = $ARGV[0] eq '-s';
    my $creator = $ARGV[1];
    my @userlist;
    foreach my $user (split(/,/, $db{"C$creator"})) {
        my $json = $db{"U$user"};
        if ($json ne '') {
            my $d = decode_json($json);
            if ($$d{'creator'} eq $creator && !&expired($d) &&
                (!$op_s || &socket_exists($user))) {
                push(@userlist, $user);
            }
        }
    }
    print encode_json(\@userlist);
} elsif ($#ARGV == 2 && $ARGV[0] eq '-q') {
    # Query: extract user data.
    my $creator = $ARGV[1];
    my $user = $ARGV[2];
    my $json = $db{"U$user"};
    if ($json ne '') {
        my $d = decode_json($json);
        if ($$d{'creator'} eq $creator && !&expired($d)) {
            delete $$d{'uid'};
            print encode_json($d);
            exit;
        }
    }
    die;
} else {
    die;
}

sub socket_exists {
    # Check whether the socket exists, which tells us whether
    # the debug container has connected.
    my ($user) = @_;
    return (-e "/comms/home/$user/sock");
}

sub new_uid {
    # We maintain a "free list" of unused UIDs.
    my $uid = $db{"I0"};
    if ($uid eq '') {
        # Database is uninitialised.
        $uid = 1000;
    }
    my $next_uid = $db{"I$uid"};
    if ($next_uid eq '') {
        $db{"I0"} = $uid + 1;
    } else {
        $db{"I0"} = $next_uid;
        delete $db{"I$uid"};
    }
    return $uid;
}

sub new_user {
    # Try with 3 digits initially, then with larger bounds, up to 15.
    # The random numbers are expected to be double-precision floats,
    # with more than 15 decimal digits of precision (52 bits).
    my $limit = 1000;
    while (1) {
        my $n = 1 + int(rand($limit - 1));
        my $json = $db{"U$n"};
        if ($json eq '') {
            return $n;
        }
        my $d = decode_json($json);
        if (&expired($d)) {
            return $n;
        }
        if ($limit < 1000000000000000) {
            $limit *= 10;
        }
    }
}

sub expired {
    my ($d) = @_;
    return $$d{'expiry'} <= time;
}

sub kill_all {
    my ($user) = @_;
    system('su', "c-$user", '-s', '/bin/sh', '-c', 'kill -9 -1');
}

sub read_user_data {
    my $x = decode_json(join('', <STDIN>));
    foreach my $f (keys %$x) {
        die unless $f =~ /^(user|expiry|ckey|skey)$/;
    }
    die unless (!defined $$x{'user'} ||
                $$x{'user'} =~ /^[a-z0-9]([-a-z0-9]{0,14}[a-z0-9])?$/);
    die unless $$x{'expiry'} =~ /^[1-9][0-9]{0,18}$/;
    die unless $$x{'ckey'} =~ /^[\x20-\x7e]{1,1000}$/;
    die unless $$x{'skey'} =~ /^[\x20-\x7e]{1,1000}$/;
    return $x;
}

sub garbage_collect {

    # Scan contents of database, deleting expired users:
    my $users = 0; # count usernames
    my %creator; # map creator to list of usernames
    my %uidname; # map UID to username
    foreach my $k (keys %db) {
        next if $db{$k} eq '';
        if ($k eq 'N') {
        } elsif ($k =~ /^U(.*)$/) {
            my $user = $1;
            my $d = decode_json($db{$k});
            if (&expired($d)) {
                delete $db{$k};
                &kill_all($user);
                system('rm', "$rdir/comms/expiry/$user");
                system('rm', '-rf', "$rdir/comms/home/$user");
            } else {
                ++$users;
                $creator{$$d{'creator'}} .= "$user,";
                $uidname{$$d{'uid'}} = $user;
            }
        } elsif ($k =~ /^C(.*)$/) {
            $creator{$1} = '';
        } elsif ($k =~ /^I[0-9]+$/) {
            delete $db{$k};
        } else {
            die;
        }
    }

    $db{'N'} = $users;

    # Update "creator" entries:
    foreach my $c (keys %creator) {
        my $v = $creator{$c};
        if ($v eq '') {
            delete $db{"C$c"};
        } else {
            my $v1 = join(',', split(/,/, $v));
            if ($db{"C$c"} ne $v1) {
                $db{"C$c"} = $v1;
            }
        }
    }

    # Update "free list" of unused UIDs:
    my @uid = sort { $a <=> $b } (keys %uidname);
    die if $#uid + 1 != $users;
    if ($#uid == -1) {
        $db{"I0"} = $uid_start;
    } else {
        my $prev = 0;
        foreach my $i ($uid_start .. $uid[$#uid] + 1) {
            if (!defined $uidname{$i}) {
                $db{"I$prev"} = $i;
                $prev = $i;
            }
        }
    }

    # Rewrite /etc/passwd:
    open(my $fd, '>', "$passwd.new") || die;
    print $fd $passwd_start;
    foreach my $uid (@uid) {
        my $user = $uidname{$uid};
        print $fd &passwd_entries($user, $uid);
    }
    close $fd;
    rename("$passwd.new", "$passwd") || die;
}

sub passwd_entries {
    my ($user, $uid) = @_;
    # Although we create /comms/home/$user for each user, the user's
    # official home directory is /. This is so that processes do not
    # have the created directory as their CWD, perhaps accidentally
    # creating a file in it or preventing it from being unlinked.
    return
        ("c-$user:x:$uid:1000::/:/comms/bin/client-shell\n" .
         "s-$user:x:$uid:1000::/:/comms/bin/server-shell\n");
}
