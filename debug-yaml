#!/usr/bin/perl

# Remote debug: script to generate Kubernetes YAML to deploy debug container.
# Copyright (c) 2020 Arm Limited
# SPDX-License-Identifier: MIT

use strict;

if ($#ARGV != -1) {
    print <<"EOF"
Usage: $0
Generate YAML for deploying the debug container.
The environment variables \$debi, \$node, \$host, \$port, \$user, \$skey,
\$dkey, \$containerid and \$namespace will be subsituted if they are set.
EOF
;
    exit;
}

my $debi = &qenv('debi', 'debug:latest');
my $node = &qenv('node');
my $host = &qenv('host');
my $port = &qenv('port');
my $user = &qenv('user');
my $skey = &qenv('skey');
my $dkey = &qenv('dkey');
my $containerid = &qenv('containerid', '');
my $namespace = &qenv('namespace', 'debug');

my $podname = ("debug-" .
               (($ENV{'user'} =~ /^[-a-z0-9]{1,16}$/) ?
                $ENV{'user'} . '-' : '') .
               &random_string(8));

my $xc = ($ENV{'containerid'} eq '') ? '#' : '';
my $xn = ($ENV{'containerid'} eq '') ? '' : '#';

sub qenv {
    my ($v, $def) = @_;
    my $n = 16; # Must be greater than the biggest indentation of a variable!
    my $r = $ENV{$v};
    if ($r eq '') {
        $r = (defined $def) ? $def : 'xx';
    }
    if ($r =~ /\n/) {
        if ($r =~ /\n$/) {
            $r = "|+\n$r";
        } else {
            $r = "|-\n$r\n";
        }
        my $i = ' ' x $n;
        $r =~ s/\n/\n$i/g;
        $r =~ s/ +$//;
    } else {
        $r =~ s/'/''/g;
        $r = "'$r'";
    }
    return $r;
}

sub random_string {
    my ($n) = @_;
    my $s = 'abcdefghijklmnopqrstuvwxyz0123456789';
    my $r = '';
    foreach my $i (1..$n) {
        $r .= substr($s, int(rand(length($s))), 1);
    }
    return $r;
}

my $yaml = <<"EOF"
apiVersion: v1
kind: Namespace
metadata:
  name: $namespace
  labels:
    name: $namespace
---
apiVersion: v1
kind: Pod
metadata:
  namespace: $namespace
  name: $podname # This must be unique.
spec:
# Uncomment if containerID is not set:
$xn  hostPID: true
  nodeName: $node # Name of node
  imagePullSecrets:
  - name: k8sedgeregcred
  containers:
  - name: unused
    image: $debi
# Uncomment if containerID is not set:
$xn    securityContext:
$xn      privileged: true
    env:
    - name: RDEBUG_HOST
      value: $host # Comms container host/address
    - name: RDEBUG_PORT
      value: $port # Comms container port
    - name: RDEBUG_USER
      value: $user # Comms container user name
    - name: RDEBUG_SKEY
      value: $skey # Server private key
    - name: RDEBUG_DKEY
      value: $dkey # Developer's public key
    - name: RDEBUG_CONTAINERID
      value: $containerid # Insert containerID, or leave empty
    volumeMounts:
    - name: host-bin
      mountPath: /bin
      readOnly: true
    - name: host-lib
      mountPath: /lib
      readOnly: true
    - name: host-lib64
      mountPath: /lib64
      readOnly: true
    - name: host-sbin
      mountPath: /sbin
      readOnly: true
    - name: host-usr
      mountPath: /usr
      readOnly: true
# Uncomment if containerID is set:
$xc    - name: host-docker-sock
$xc      mountPath: /var/run/docker.sock
  volumes:
  - name: host-bin
    hostPath:
      path: /bin
  - name: host-lib
    hostPath:
      path: /lib
  - name: host-lib64
    hostPath:
      path: /lib64
  - name: host-sbin
    hostPath:
      path: /sbin
  - name: host-usr
    hostPath:
      path: /usr
  - name: host-docker-sock
    hostPath:
      path: /var/run/docker.sock
EOF
    ;
print $yaml;
